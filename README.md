# my_yaml_collections

A collection of YAML for k3s. The each YAML will use for some specific purpose on my need. 
Some are collected from the Internet, and some tested by me. 
Feel free to download and use for your need. 

## Debugging 
* **share-process-namespace.yaml**: This is used for testing the share-process-namespace function
